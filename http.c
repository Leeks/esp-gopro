#include "ets_sys.h"
#include "osapi.h"
#include "os_type.h"
#include "user_interface.h"
#include "mem.h"
#include "espconn.h"


struct espconn tcp_conn;
struct _esp_tcp tcp;
ip_addr_t tcp_server_ip;
const char *path;
const char *host;

static void ICACHE_FLASH_ATTR tcp_recv_cb(void *arg,
					  char *data,
					  unsigned short len)
{
  os_printf("<%s>\n", __FUNCTION__);

  struct espconn *pespconn = arg;
  espconn_disconnect(pespconn);
}


static void ICACHE_FLASH_ATTR tcp_sent_cb(void *arg)
{
  os_printf("<%s>\n", __FUNCTION__);

}


static void ICACHE_FLASH_ATTR tcp_discon_cb(void *arg)
{
  os_printf("<%s>\n", __FUNCTION__);
}


static void ICACHE_FLASH_ATTR tcp_connect_cb(void *arg)
{
  os_printf("<%s>\n", __FUNCTION__);


  struct espconn *pespconn = arg;

  char *pbuf = (char *)os_zalloc(2048);
  os_sprintf(pbuf, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n",
	     path, host);
  espconn_send(pespconn, pbuf, os_strlen(pbuf));
  os_free(pbuf);
}


static void ICACHE_FLASH_ATTR dns_found_cb(const char *name,
				    ip_addr_t *ipaddr,
				    void *arg)
{
  os_printf("<%s> %s\n", __FUNCTION__, name);

  struct espconn *pespconn = (struct espconn *) arg;

  if (ipaddr == NULL) {
    os_printf("<%s> DNS Lookup failed\n", __FUNCTION__);
    return;
  }

  // print ip addr
  os_printf("<%s> IP found %d.%d.%d.%d \r\n", __FUNCTION__,
	    *((uint8 *)&ipaddr->addr), *((uint8 *)&ipaddr->addr + 1),
	    *((uint8 *)&ipaddr->addr + 2), *((uint8 *)&ipaddr->addr + 3));

  if (tcp_server_ip.addr == 0 && ipaddr->addr != 0) {
    tcp_server_ip.addr = ipaddr->addr;
    os_memcpy(pespconn->proto.tcp->remote_ip, &ipaddr->addr, 4);
    pespconn->proto.tcp->remote_port = 80;
    pespconn->proto.tcp->local_port = espconn_port();

    espconn_regist_connectcb(pespconn, tcp_connect_cb);
    // espconn_regist_reconcb(pespconn, tcp_recon_cb);

    espconn_connect(pespconn);
  }
}

static struct espconn * ICACHE_FLASH_ATTR espconn_tcp_new()
{
  os_printf("<%s>\n", __FUNCTION__);

}

void ICACHE_FLASH_ATTR http_recv_print_cb(void *arg,
					  char *data,
					  unsigned short len)
{
  os_printf(data);
  os_printf("\n");
}

void ICACHE_FLASH_ATTR http_get(const char *http_host,
				const char *http_path,
				espconn_recv_callback cb)
{
  os_printf("<%s> %s%s\n", __FUNCTION__, http_host, http_path);

  tcp_conn.proto.tcp = &tcp;
  tcp_conn.type = ESPCONN_TCP;
  tcp_conn.state = ESPCONN_NONE;
  tcp_server_ip.addr = 0;
  host = http_host;
  path = http_path;
  
  if (cb != NULL) {
    espconn_regist_recvcb(&tcp_conn, cb);
  } else {
    espconn_regist_recvcb(&tcp_conn, tcp_recv_cb);
  }
  
  espconn_regist_sentcb(&tcp_conn, tcp_sent_cb);
  espconn_regist_disconcb(&tcp_conn, tcp_discon_cb);

  espconn_gethostbyname(&tcp_conn, http_host, &tcp_server_ip, dns_found_cb);
}

void ICACHE_FLASH_ATTR gopro_get(const char *http_path,
				 espconn_recv_callback cb)
{
  static const char GOPRO_IP[4] = {10, 5, 5, 9};

  os_printf("<%s> %s\n", __FUNCTION__, http_path);

  tcp_conn.proto.tcp = &tcp;
  tcp_conn.type = ESPCONN_TCP;
  tcp_conn.state = ESPCONN_NONE;
  tcp_server_ip.addr = 0;
  host = "10.5.5.9";
  path = http_path;
  
  os_memcpy(tcp_conn.proto.tcp->remote_ip, GOPRO_IP, 4);
  tcp_conn.proto.tcp->remote_port = 80;
  tcp_conn.proto.tcp->local_port = espconn_port();

  if (cb != NULL) {
    espconn_regist_recvcb(&tcp_conn, cb);
  } else {
    espconn_regist_recvcb(&tcp_conn, tcp_recv_cb);
  }
  
  espconn_regist_sentcb(&tcp_conn, tcp_sent_cb);
  espconn_regist_connectcb(&tcp_conn, tcp_connect_cb);
  espconn_regist_disconcb(&tcp_conn, tcp_discon_cb);

  espconn_connect(&tcp_conn);
}
