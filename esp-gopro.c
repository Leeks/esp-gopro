#include "ets_sys.h"
#include "osapi.h"
#include "gpio.h"
#include "os_type.h"
#include "user_interface.h"
#include "mem.h"

#include "gopro_consts.h"
#include "http.h"

#define INPUT_PIN_NO 0
#define READ_INPUT_INTERVAL 50
#define NO_INPUT_TIMEOUT 1000

// Should not need to edit
#define CONSEQ_HIGH_LIMIT (NO_INPUT_TIMEOUT / READ_INPUT_INTERVAL)


static volatile os_timer_t read_input_timer;
static volatile os_timer_t wifi_check_connection_timer;

char buffer[256];
const char *tcp_connect_cb_url_path;

bool ICACHE_FLASH_ATTR connect_to_wifi()
{
  os_printf("<%s>\n", __FUNCTION__);

  static const char ssid[32] = "EinFarht";
  static const char password[32] = "goproslow";
  
  static struct station_config stationConfig;
  
  wifi_set_opmode(STATION_MODE);
  os_memcpy(&stationConfig.ssid, ssid, 32);
  os_memcpy(&stationConfig.password, password, 32);

  wifi_station_set_config(&stationConfig);

  os_printf("Connecting...");
}


void ICACHE_FLASH_ATTR wifi_connected_cb()
{
  os_printf("<%s>\n", __FUNCTION__);
}

void ICACHE_FLASH_ATTR wifi_check_connection(void *arg)
{
  uint8 connection_status = wifi_station_get_connect_status();
  os_printf("<%s> Status: %d\n", __FUNCTION__, connection_status);

  if (connection_status == STATION_GOT_IP) {
    os_timer_disarm(&wifi_check_connection_timer);
    wifi_connected_cb();
  }
}


void ICACHE_FLASH_ATTR clear_intr_status(void)
{
  uint32 gpio_status;
  gpio_status = GPIO_REG_READ(GPIO_STATUS_ADDRESS);
  GPIO_REG_WRITE(GPIO_STATUS_W1TC_ADDRESS, gpio_status);
}

void ICACHE_FLASH_ATTR handle_presses(int num_presses)
{
  os_printf("<%s> %d ", __FUNCTION__, num_presses);

  switch (num_presses) {
  case 1:
    os_printf("tag moment\n");
    gopro_get(GOPRO_TAG_MOMENT_PATH, NULL);
    break;
  case 2:
    os_printf("start record\n");
    gopro_get(GOPRO_START_RECORD_PATH, NULL);
    break;
  case 3:
    os_printf("stop record\n");
    gopro_get(GOPRO_STOP_RECORD_PATH, NULL);
    break;
  }
}

/**
 * Polls pin via timer
 **/
void ICACHE_FLASH_ATTR read_input(void *arg)
{
  static bool trigger_down_on_next = true;
  static int key_state = 1;
  static int conseq_high = -1;
  static int num_presses = 0;

  int level = GPIO_INPUT_GET(INPUT_PIN_NO);

  if (level == 1) {
    conseq_high++;
  } else {
    conseq_high = 0;
  }

  if (key_state == 0 && level == 1) {
    os_printf("<%s> key_state: %s\n", __FUNCTION__, "UP");
    key_state = 1;
  } else if (trigger_down_on_next || key_state == 1 && level == 0) {
    os_printf("<%s> key_state: %s\n", __FUNCTION__, "DOWN");
    num_presses++;
    key_state = 0;
    trigger_down_on_next = false;
  }

  // If no input for a while
  if (conseq_high > CONSEQ_HIGH_LIMIT) {
    os_printf("<%s> EXIT: %d\n", __FUNCTION__, num_presses);

    handle_presses(num_presses);

    // reset static
    trigger_down_on_next = true;
    conseq_high = 0;
    num_presses = 0;

    // disable timer
    os_timer_disarm(&read_input_timer);
    clear_intr_status();

    // wait for next interrupt to call this function again
    ETS_GPIO_INTR_ENABLE();
  }
}

/**
 * Enables read pin timer on first pin interrupt.
 **/
void ICACHE_FLASH_ATTR read_input_intr_cb(void *arg)
{
  ETS_GPIO_INTR_DISABLE();
  os_printf("<%s>\n", __FUNCTION__);
  os_timer_arm(&read_input_timer, READ_INPUT_INTERVAL, 1);
}

void ICACHE_FLASH_ATTR init_timers(void)
{
  os_printf("<%s>\n", __FUNCTION__);
  os_timer_setfn(&read_input_timer, read_input, NULL);
  os_timer_setfn(&wifi_check_connection_timer, wifi_check_connection, NULL);
  os_timer_arm(&wifi_check_connection_timer, 500, 1);
}

void ICACHE_FLASH_ATTR init_peripherals(void)
{
  os_printf("<%s>\n", __FUNCTION__);

  gpio_init();
  
  // pull up inputs
  PIN_PULLUP_EN(PERIPHS_IO_MUX_MTDI_U);
  PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTDI_U, FUNC_GPIO0);

  // set input pin
  GPIO_DIS_OUTPUT(INPUT_PIN_NO);

  // listen for interrupt on input pin
  ETS_GPIO_INTR_DISABLE();
  ETS_GPIO_INTR_ATTACH(read_input_intr_cb, INPUT_PIN_NO);
  GPIO_REG_WRITE(GPIO_STATUS_W1TC_ADDRESS, BIT(INPUT_PIN_NO));
  gpio_pin_intr_state_set(GPIO_ID_PIN(INPUT_PIN_NO), GPIO_PIN_INTR_ANYEDGE);
  ETS_GPIO_INTR_ENABLE();
}

void ICACHE_FLASH_ATTR tcp_get(const char *proto,
			       const char *resource_name,
			       const char *filepath,
			       char *buffer)
{
  //
}


void ICACHE_FLASH_ATTR user_init()
{
  uart_div_modify(0, UART_CLK_FREQ / 115200);

  connect_to_wifi("Einfahrt", "goproslow");
  init_timers();
  init_peripherals();
}


