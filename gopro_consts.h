#ifndef GOPRO_CONSTS_H_
#define GOPRO_CONSTS_H_

const char GOPRO_IP[4] = {10, 5, 5, 9};

#define GOPRO_VIDEO_MODE_PATH "/gp/gpControl/setting/10/1"
#define GOPRO_START_RECORD_PATH "/gp/gpControl/command/shutter?p=1"
#define GOPRO_TAG_MOMENT_PATH "/gp/gpControl/command/storage/tag_moment"
#define GOPRO_STOP_RECORD_PATH "/gp/gpControl/command/shutter?p=0"

#endif
