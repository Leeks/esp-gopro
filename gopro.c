#include "user_interface.h"
#include "osapi.h"
#include "mem.h"
#include "espconn.h"

#define GOPRO_IP "10.5.5.9"
#define GET_REQUEST_TEMPLATE "GET %s HTTP/1.1\r\nHost: " GOPRO_IP "\r\n\r\n"

const char *VIDEO_MODE_PATH = "gp/gpControl/setting/10/1";
const char *START_RECORD_PATH = "gp/gpControl/command/shutter?p=1";
const char *TAG_MOMENT_PATH = "gp/gpControl/command/storage/tag_moment";
const char *STOP_RECORD_PATH = "gp/gpControl/command/shutter?p=1";

/**
 * Callback for data recieved in response to sent data
 */
void ICACHE_FLASH_ATTR my_recvcb(void *arg, char *pdata, unsigned short len)
{
  struct espconn *pconn = arg;
  
  // print received data
  os_printf("<%s> %s\r\n", __FUNCTION__, pdata);

  // disconnect
  espconn_disconnect(pconn);
}

/**
 * Callback for when tcp connection is closed
 */
void ICACHE_FLASH_ATTR my_disconcb(void *arg)
{
  struct espconn *pconn = arg;

  // free the connection
  os_free(pconn->proto.tcp);
  os_free(pconn);
}

struct espconn* ICACHE_FLASH_ATTR new_conn_to_ip(const char *ip)
{
  struct espconn *pconn = os_zalloc(sizeof(*pconn));

  pconn->type = ESPCONN_TCP;
  pconn->state = ESPCONN_NONE;

  // configure tcp
  esp_tcp *ptcp = os_zalloc(sizeof(*ptcp));
  ip_addr_t *paddr = os_zalloc(sizeof(*paddr));
  ipaddr_aton(ip, paddr); // might fail
  os_memcpy(ptcp->remote_ip, paddr->addr, 4);
  os_free(paddr);
  ptcp->remote_port = 80;
  ptcp->local_port = espconn_port();
  pconn->proto.tcp = ptcp;
  
  return pconn;
}

/**
 * Create a connection to gopro with specified callback
 */
void ICACHE_FLASH_ATTR gopro_connect(espconn_connect_callback connect_cb)
{
  struct espconn *pconn = new_conn_to_ip(GOPRO_IP);
  espconn_regist_connectcb(pconn, connect_cb);
  espconn_connect(pconn);
}

/**
 * Send get request for path on connection
 */
void ICACHE_FLASH_ATTR conn_send_get(struct espconn *pconn, const char *path)
{
  espconn_regist_recvcb(pconn, my_recvcb);
  espconn_regist_disconcb(pconn, my_disconcb);

  char *pbuf = os_zalloc(256);
  os_sprintf(pbuf, GET_REQUEST_TEMPLATE, VIDEO_MODE_PATH);
  espconn_send(pconn, pbuf, os_strlen(pbuf));
  os_free(pbuf);
} 

void ICACHE_FLASH_ATTR gopro_video_mode_cb(void *arg)
{
  os_printf("<%s>", __FUNCTION__);

  struct espconn *pconn = arg;
  conn_send_get(pconn, VIDEO_MODE_PATH);
}

void ICACHE_FLASH_ATTR gopro_start_record_cb(void *arg)
{
  os_printf("<%s>", __FUNCTION__);

  struct espconn *pconn = arg;
  conn_send_get(pconn, START_RECORD_PATH);
}

void ICACHE_FLASH_ATTR gopro_tag_moment_cb(void *arg)
{
  os_printf("<%s>", __FUNCTION__);

  struct espconn *pconn = arg;
  conn_send_get(pconn, TAG_MOMENT_PATH);
}

void ICACHE_FLASH_ATTR gopro_stop_record_cb(void *arg)
{
  os_printf("<%s>", __FUNCTION__);

  struct espconn *pconn = arg;
  conn_send_get(pconn, STOP_RECORD_PATH);
}
