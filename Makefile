CC = xtensa-lx106-elf-gcc
CFLAGS = -I. -DICACHE_FLASH -mlongcalls
LDLIBS = -nostdlib -Wl,--start-group -lmain -lnet80211 -lwpa -llwip -lpp -lphy -Wl,--end-group -lgcc -lc 
LDFLAGS = -Teagle.app.v6.ld

esp-gopro-0x00000.bin: esp-gopro
	esptool.py elf2image $^

esp-gopro: esp-gopro.o gopro.o http.o gopro_consts.o

esp-gopro.o: esp-gopro.c

flash: esp-gopro-0x00000.bin
	esptool.py write_flash 0 esp-gopro-0x00000.bin 0x10000 esp-gopro-0x10000.bin

clean:
	rm -f esp-gopro \
              esp-gopro.o \
              esp-gopro-0x00000.bin \
              esp-gopro-0x10000.bin \
              gopro.o \
              http.o \
              gopro_consts.o
