#ifndef HTTP_H_
#define HTTP_H_

#include "espconn.h"


void ICACHE_FLASH_ATTR http_get(const char *host,
				const char *path,
				espconn_recv_callback cb);

void ICACHE_FLASH_ATTR gopro_get(const char *http_path,
				 espconn_recv_callback cb);

void ICACHE_FLASH_ATTR http_recv_print_cb(void *arg,
					  char *data,
					  unsigned short len);
#endif
