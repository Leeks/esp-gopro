#ifndef GOPRO_H_
#define GOPRO_H_

#include "espconn.h"

extern const char *GOPRO_IP;
extern const char *VIDEO_MODE_PATH;
extern const char *START_RECORD_PATH;
extern const char *TAG_MOMENT_PATH;
extern const char *STOP_RECORD_PATH;

void ICACHE_FLASH_ATTR gopro_connect(espconn_connect_callback connect_cb);
void ICACHE_FLASH_ATTR gopro_video_mode_cb(void *arg);
void ICACHE_FLASH_ATTR gopro_start_record_cb(void *arg);
void ICACHE_FLASH_ATTR gopro_tag_moment_cb(void *arg);
void ICACHE_FLASH_ATTR gopro_stop_record_cb(void *arg);

#endif
